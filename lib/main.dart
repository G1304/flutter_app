import 'package:flutter/material.dart';
import 'package:first_prjct/pages/todos.dart';
import 'package:first_prjct/pages/menu.dart';
import 'package:first_prjct/pages/album.dart';
import 'package:first_prjct/pages/fetchtodos.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => Menu(),
        '/todos': (context) => Todos(),
        '/album': (context) => Album(),
        '/fetchtodos': (context) => FetchTodos(),
      },
    ));
