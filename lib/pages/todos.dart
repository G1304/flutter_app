import 'package:flutter/material.dart';

class Todos extends StatefulWidget {
  const Todos({Key? key}) : super(key: key);

  @override
  _TodosState createState() => _TodosState();
}

class _TodosState extends State<Todos> {
  String _singleTodo = '';
  List todoList = [];

  @override
  void initState() {
    super.initState();
    todoList.addAll(['Learn Flutter', 'Learn Dart']);
  }

  void _openMenu() => {Navigator.pushReplacementNamed(context, "/")};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        title: Text("Todo List"),
        centerTitle: true,
        backgroundColor: Colors.red[800],
        actions: [
          IconButton(onPressed: _openMenu, icon: Icon(Icons.arrow_back_sharp))
        ],
      ),
      body: SafeArea(
        child: ListView.builder(
            itemCount: todoList.length,
            itemBuilder: (BuildContext context, int index) {
              return Dismissible(
                key: Key(todoList[index]),
                child: Card(
                  child: ListTile(
                    title: Text(todoList[index]),
                    trailing: IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () {
                        setState(() {
                          todoList.removeAt(index);
                        });
                      },
                    ),
                  ),
                ),
                onDismissed: (direction) {
                  setState(() {
                    todoList.removeAt(index);
                  });
                },
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.redAccent,
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                    title: Center(child: Text("Add To-do")),
                    content: TextField(
                      onChanged: (String value) {
                        _singleTodo = value;
                      },
                    ),
                    actions: [
                      Center(
                          child: ElevatedButton(
                        child: Text("Add"),
                        onPressed: () {
                          setState(() {
                            if (_singleTodo != '') {
                              todoList.add(_singleTodo);
                              _singleTodo = '';
                              Navigator.of(context).pop();
                            } else {
                              Navigator.of(context).pop();
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(const SnackBar(
                                      content: Text(
                                "Please enter To-do",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              )));
                            }
                          });
                        },
                      ))
                    ]);
              });
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
