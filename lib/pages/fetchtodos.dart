import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

Future<List<SingleTodo>> fetchTodos() async {
  final response =
      await http.get(Uri.parse('https://jsonplaceholder.typicode.com/todos'));

  if (response.statusCode == 200) {
    return parseTodos(response.body);
  } else {
    throw Exception('Failed to load..');
  }
}

List<SingleTodo> parseTodos(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<SingleTodo>((json) => SingleTodo.fromJson(json)).toList();
}

class SingleTodo {
  final int userId;
  final int id;
  final String title;
  final bool completed;

  SingleTodo({
    required this.userId,
    required this.id,
    required this.title,
    required this.completed,
  });

  factory SingleTodo.fromJson(Map<String, dynamic> json) {
    return SingleTodo(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      completed: json['completed'],
    );
  }
}

class FetchTodos extends StatefulWidget {
  const FetchTodos({Key? key}) : super(key: key);

  @override
  _FetchTodosState createState() => _FetchTodosState();
}

class _FetchTodosState extends State<FetchTodos> {
  late Future<SingleTodo> futureTodo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[600],
      appBar: AppBar(
        backgroundColor: Colors.red[800],
        title: Text("Fetch Todos"),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () => Navigator.pushReplacementNamed(context, '/'),
              icon: Icon(Icons.arrow_back_sharp))
        ],
      ),
      body: SafeArea(
        child: FutureBuilder<List<SingleTodo>>(
          future: fetchTodos(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return TodoList(todos: snapshot.data!);
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}

class TodoList extends StatelessWidget {
  const TodoList({Key? key, required this.todos}) : super(key: key);
  final List<SingleTodo> todos;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemCount: todos.length,
        itemBuilder: (context, index) {
          return Center(
            child: Text(
              "Title: \n${todos[index].title}\nCompleted: ${todos[index].completed}\n"
              "ID: ${todos[index].id}\nuserId: ${todos[index].userId}",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
                color: Colors.white,
              ),
            ),
          );
        });
  }
}
